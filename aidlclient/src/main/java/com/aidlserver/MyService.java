package com.aidlserver;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import com.aidl.IMyAidlInterface;

public class MyService extends Service {
    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new myImpl();
    }

    private class myImpl extends IMyAidlInterface.Stub{
        @Override
        public int calculate(int num1, int num2) throws RemoteException {
            return num1+num2;
        }
    }
}
