package com.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.service.service.MessangerBindingService;

public class MessangerBindingActivity extends AppCompatActivity implements View.OnClickListener {
    private Button mButtonStartService, mButtonStopService, mButtonBindService;
    private Messenger messenger = null;
    private boolean isBind = false;

    //component that can bind service is activity,service and content provide
    //you can not bind service in broadcastreciever
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_messangerbinding);
        mButtonStartService = (Button) findViewById(R.id.button_start_service);
        mButtonStopService = (Button) findViewById(R.id.button_stop_service);
        mButtonBindService = (Button) findViewById(R.id.button_bind_service);

        mButtonStartService.setOnClickListener(this);
        mButtonStopService.setOnClickListener(this);
        mButtonBindService.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_start_service:
                Intent intent = new Intent(this, MessangerBindingService.class);
                bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
                break;
            case R.id.button_stop_service:
                try {
                    Message message = Message.obtain(null, MessangerBindingService.JOB_1);
                    messenger.send(message);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.button_bind_service:
                try {
                    Message message = Message.obtain(null, MessangerBindingService.JOB_2);
                    messenger.send(message);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;

        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            messenger = new Messenger(service);
            isBind = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBind = false;
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
