package com.service.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

//work on main thread
//need to call stop service
public class SimpleService extends Service {
    private String TAG ="Service";
    @Override
    public int onStartCommand(Intent intent,  int flags, int startId) {
        Log.d(TAG,"onStartCommand");
        return START_STICKY;
    }

    //called only once when starting service multiple times
    @Override
    public void onCreate() {
        Log.d(TAG,"onCreate");
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG,"onBind");
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG,"onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG,"onDestory");
        super.onDestroy();
    }
}
