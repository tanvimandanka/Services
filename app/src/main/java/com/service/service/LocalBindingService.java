package com.service.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Random;

public class LocalBindingService extends Service {
    private int mRandomNumber;
    private boolean mIsRandomNumberGeneratorOn;

    private final int MIN = 0;
    private final int MAX = 100;
    private IBinder iBinder = new ServiceBinder();


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service", "Thread Id onStartCommand " + Thread.currentThread().getId() + "");
        mIsRandomNumberGeneratorOn = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                startRandomNumberGenerator();
            }
        }).start();
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    //you can bind service which is not started
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("Service", "onBind");
        return iBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("Service", "onUnBind");
        return super.onUnbind(intent);
    }

    private void startRandomNumberGenerator() {
        while (mIsRandomNumberGeneratorOn) {
            try {
                Thread.sleep(1000);
                mRandomNumber = new Random().nextInt(MAX) + MIN;
                Log.d("Service", "Thread Id  " + Thread.currentThread().getId() + "");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void stopRandomNumberGenerator() {
        mIsRandomNumberGeneratorOn = false;
    }

    public int getRandomNumber() {
        return mRandomNumber;
    }

    //bound service can not be destroyed
    @Override
    public void onDestroy() {
        Log.d("Service", "onDestroyed");
        super.onDestroy();
        stopRandomNumberGenerator();
    }

    //extends binder of implements IBinder
    public class ServiceBinder extends Binder {
        public LocalBindingService getService() {
            return LocalBindingService.this;
        }
    }
}
