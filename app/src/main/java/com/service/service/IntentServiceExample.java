package com.service.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

//work on main thread
//need to call stop service
public class IntentServiceExample extends IntentService {
    private String TAG = "Service";

    public IntentServiceExample() {
        super("hey");
    }

    //system calls when the IntentService receives a start request."
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d(TAG, "onHandleIntent");
    }
}
