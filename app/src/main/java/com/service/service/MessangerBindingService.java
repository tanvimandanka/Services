package com.service.service;


import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.widget.Toast;

public class MessangerBindingService extends Service {
    public static final int JOB_1 = 1;
    public static final int JOB_2 = 2;

    private Messenger messenger = new Messenger(new IncomingHandler());

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case JOB_1:
                    try {
                        Bundle bundle = new Bundle();
                        bundle.putString("name","Tanvi");
                        msg.replyTo.send(Message.obtain(null,JOB_1,bundle));
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    break;
                case JOB_2:
                    Toast.makeText(getApplicationContext(), "Hello World Again", Toast.LENGTH_SHORT).show();
                    break;
            }

            super.handleMessage(msg);
        }
    }
}
