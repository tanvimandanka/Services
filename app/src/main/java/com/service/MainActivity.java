package com.service;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.service.service.LocalBindingService;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Intent mIntent;
    private ServiceConnection mServiceConnection;
    private boolean isServiceBound;
    private TextView mTextView;
    private Button mButtonStartService, mButtonStopService,
            mButtonBindService, mButtonUnBindeService, mButtonGetRandomNumber;
    private LocalBindingService mLocalBindingService;

    //component that can bind service is activity,service and content provide
    //you can not bind service in broadcastreciever
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextView = (TextView) findViewById(R.id.text_result);
        mButtonStartService = (Button) findViewById(R.id.button_start_service);
        mButtonStopService = (Button) findViewById(R.id.button_stop_service);
        mButtonBindService = (Button) findViewById(R.id.button_bind_service);
        mButtonUnBindeService = (Button) findViewById(R.id.button_unbind_service);
        mButtonGetRandomNumber = (Button) findViewById(R.id.button_random_number);
        mButtonStartService.setOnClickListener(this);
        mButtonStopService.setOnClickListener(this);
        mButtonBindService.setOnClickListener(this);
        mButtonUnBindeService.setOnClickListener(this);
        mButtonGetRandomNumber.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_start_service:
                mIntent = new Intent(this, LocalBindingService.class);
                startService(mIntent);
                break;
            case R.id.button_stop_service:
                stopService(mIntent);
                break;
            case R.id.button_bind_service:
                bindService();
                break;
            case R.id.button_unbind_service:
                if (isServiceBound) {
                    unbindService(mServiceConnection);
                    isServiceBound = false;
                }
                break;
            case R.id.button_random_number:
                if (isServiceBound) {
                    mTextView.setText(mLocalBindingService.getRandomNumber() + "");
                } else {
                    mTextView.setText("Service not bound");
                }
        }
    }

    public void bindService() {
        if (mServiceConnection == null) {
            mServiceConnection = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    isServiceBound = true;
                    LocalBindingService.ServiceBinder serviceBinder = (LocalBindingService.ServiceBinder) service;
                    mLocalBindingService = serviceBinder.getService();
                }

                @Override
                public void onServiceDisconnected(ComponentName name) {
                    isServiceBound = false;
                }
            };
        }
        bindService(mIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
